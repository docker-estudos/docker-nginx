# Download base image
FROM nginx

# INFO
LABEL maintainer="peixoto.leopoldo@outlook.com"
LABEL version="0.2"
LABEL description = "This is custom Docker Image for Leopoldo"

# Cópia de arquivo local para a imagem do container
COPY ./html/index.html /usr/share/nginx/html/index.html

# Expose Port for the application
EXPOSE 80